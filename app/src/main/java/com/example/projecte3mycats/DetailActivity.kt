package com.example.projecte3mycats

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.getValue
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.Alignment
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.projecte3mycats.ui.model.CatsUIModel
import com.example.projecte3mycats.ui.screens.DetailViewModel
import com.example.projecte3mycats.ui.theme.Projecte3MyCatsTheme

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val catId= intent.getStringExtra("catsId")
            catId?.let { CatsApp(catId = it) }
        }
    }
}
@Composable
fun CatsApp(catId: String){
    Projecte3MyCatsTheme {
        CatsDetail(catId)
    }

}
@Composable
fun CatsDetail(catId: String, detailViewModel: DetailViewModel = viewModel()) {
    val uiState by detailViewModel.uiState.collectAsState()
    detailViewModel.getCatsDataFromId(catId)

    uiState?.let {uiState ->
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .background(Color.LightGray)
        ) {
            Text(
                text = uiState.name,
                fontSize = 20.sp,
                fontWeight = FontWeight.ExtraBold,
                modifier = Modifier.padding(bottom = 20.dp, start = 20.dp, end = 20.dp, top = 20.dp)
            )
            Log.i("Hello", uiState.img_url)
            Image(
                painter = rememberAsyncImagePainter(uiState.img_url),
                contentDescription = "no image founded",
                modifier = Modifier
                    .size(300.dp)
                    .clip(RoundedCornerShape(100))
                    //.scale(2.5f)
                    //.padding(top = 100.dp, bottom = 100.dp)
                    //.clip(RoundedCornerShape(100))
            )
            Text(
                text = "DESCRIPTION: "+uiState.description,
                fontSize = 14.sp,
                modifier = Modifier.padding(bottom = 20.dp, start = 20.dp, end = 20.dp, top = 30.dp)
            )
            Text(
                text = "COUNTRY CODE: "+uiState.countryCode,
                fontSize = 14.sp,
                modifier = Modifier.padding(bottom = 20.dp, start = 20.dp, end = 20.dp, top = 30.dp)
            )
            Text(
                text = "TEMPERAMENT: "+uiState.temperament,
                fontSize = 14.sp,
                modifier = Modifier.padding(bottom = 20.dp, start = 20.dp, end = 20.dp, top = 30.dp)
            )
            Text(
                text = "WIKIPEDIA URL: "+uiState.wikipedia_url,
                fontSize = 14.sp,
                modifier = Modifier.padding(bottom = 20.dp, start = 20.dp, end = 20.dp, top = 30.dp)
            )
        }
    }
}

@Preview
@Composable
fun viewThis(){
    Image(
        painter = rememberAsyncImagePainter("https://cdn2.thecatapi.com/images/gCEFbK7in.jpg"),
        contentDescription = "no image founded",
        modifier = Modifier
            .clip(RoundedCornerShape(50))
    )
}