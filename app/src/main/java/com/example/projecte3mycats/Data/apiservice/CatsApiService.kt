package com.example.projecte3mycats.Data.apiservice

import com.example.projecte3mycats.Data.apiservice.model.CatImageDto
import com.example.projecte3mycats.Data.apiservice.model.CatsDto
import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.http.Path
import retrofit2.http.Query


private const val BASE_URL =
    "https://api.thecatapi.com"

private val retrofit = Retrofit.Builder()
    .addConverterFactory(Json { ignoreUnknownKeys = true}.asConverterFactory(("application/json").toMediaType()))
    .baseUrl(BASE_URL)
    .build()

object CatsApi{
    val retrofitService : CatsApiService by lazy{
        retrofit.create(CatsApiService::class.java)
    }
}
interface CatsApiService{
    @GET("/v1/breeds")
    suspend fun getCats(): List<CatsDto>
    @GET("/v1/images/search")
    suspend fun getCatImageRx(@Query("breed_id")id:String) : List<CatImageDto>
    @GET("/v1/breeds/{breed_id}")
    suspend fun getBreed(@Path("breed_id") breed_id: String): CatsDto
}