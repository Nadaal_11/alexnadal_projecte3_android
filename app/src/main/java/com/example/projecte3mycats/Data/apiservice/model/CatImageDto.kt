package com.example.projecte3mycats.Data.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CatImageDto(
    @SerialName("url") val imageUrl: String =""
)
