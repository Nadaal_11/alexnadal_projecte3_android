package com.example.projecte3mycats

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberAsyncImagePainter
import com.example.marsphotos.R
import com.example.projecte3mycats.ui.model.CatsUIModel
import com.example.projecte3mycats.ui.screens.CatsViewModel
import com.example.projecte3mycats.ui.theme.Projecte3MyCatsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Projecte3MyCatsTheme {
                CatsApp()
            }
        }
    }
}
private const val TAG = "MainActivity"

@Composable
fun CatsApp(catsViewModel: CatsViewModel = viewModel()){
    val uiState by catsViewModel.uiState.collectAsState()
    CatsList(catsList = uiState)
}

@Composable
fun CatsList(catsList: List<CatsUIModel>, modifier: Modifier = Modifier){
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(16.dp)
    ) {
        items(catsList.count()){ element ->
            CatCard(catsUIModel = catsList[element])
            Spacer(modifier = Modifier.size(15.dp))
        }
    }
}
@Composable
fun ExtensableButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = if(expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = null
        )
    }
}
@Composable
fun CatCard(catsUIModel: CatsUIModel, modifier: Modifier = Modifier){
    var expanded by remember{
        mutableStateOf(false)
    }
    Card(
        backgroundColor = Color.LightGray,
        shape = RoundedCornerShape(15.dp),
        modifier = Modifier.fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .size(110.dp)
                        .padding(horizontal = 8.dp)
                        .clip(RoundedCornerShape(100)),
                    painter = rememberAsyncImagePainter(catsUIModel.img_url),
                    contentDescription = null
                )
                Text(
                    text = catsUIModel.name,
                    modifier = Modifier.weight(1f)
                )
                ExtensableButton(expanded = expanded, onClick = {expanded = !expanded})
            }
            if (expanded) {
                DescriptionExtendedCard(catsUIModel.description, catsUIModel.id)
            }
        }
    }
}
@Composable
fun DescriptionExtendedCard(description: String, catsId: String) {
    Column(
        modifier = Modifier.padding(
            start = 16.dp,
            top = 8.dp,
            bottom = 16.dp,
            end = 16.dp
        )
    ) {
        Text(
            text = description,
            overflow = TextOverflow.Ellipsis,
            maxLines = 2
        )
    }
    Spacer(modifier = Modifier)
    val mContext = LocalContext.current

    Button(
        onClick = {
            mContext.startActivity(Intent(mContext,DetailActivity::class.java).putExtra("catsId",catsId))
        },
        modifier = Modifier
            .padding(15.dp),
        colors = ButtonDefaults.buttonColors(colorResource(id = R.color.teal_700))
    ) {
        Text(text = "See more", fontSize = 14.sp, fontWeight = FontWeight.Bold)

    }
}