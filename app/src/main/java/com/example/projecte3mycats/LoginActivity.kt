package com.example.projecte3mycats

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.*
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.*
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.*

class LoginActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // Variables para almacenar el nombre y el estado del botón
            var name by remember { mutableStateOf(TextFieldValue()) }
            var passwd by remember { mutableStateOf(TextFieldValue()) }
            var buttonEnabled by remember { mutableStateOf(false) }
            var buttonEnabled2 by remember { mutableStateOf(false) }

            // Variable para manejar el foco en el TextField
            val focusRequester = remember { FocusRequester() }

            // Diseño de la pantalla de inicio de sesión
            Scaffold(
                content = {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(16.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        OutlinedTextField(
                            value = name,
                            onValueChange = {
                                // Actualizar el valor del nombre y habilitar el botón si el nombre tiene al menos un carácter
                                name = it
                                buttonEnabled = it.text.isNotEmpty()
                            },
                            label = { Text(text = "Nombre") },
                            modifier = Modifier.fillMaxWidth(),
                            singleLine = true,
                            keyboardActions = KeyboardActions(onDone = { focusRequester.requestFocus() })
                        )
                        OutlinedTextField(
                            value = passwd,
                            onValueChange = {
                                // Actualizar el valor del password y habilitar el botón si el nombre tiene al menos un carácter
                                passwd = it
                                buttonEnabled2 = it.text.isNotEmpty()
                            },
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Password,
                                imeAction = ImeAction.Done
                            ),
                            visualTransformation = PasswordVisualTransformation(),
                            label = { Text(text = "Password") },
                            modifier = Modifier.fillMaxWidth(),
                            singleLine = true,
                            keyboardActions = KeyboardActions(onDone = { focusRequester.requestFocus() })
                        )
                        Button(
                            onClick = {
                                // Iniciar la siguiente actividad cuando se hace clic en el botón de inicio
                                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                                startActivity(intent)
                            },
                            enabled = buttonEnabled && buttonEnabled2,
                            colors = ButtonDefaults.buttonColors(backgroundColor = if (buttonEnabled and buttonEnabled2) MaterialTheme.colors.primary else Color.LightGray),
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Text(text = "Iniciar")
                        }
                    }
                }
            )
        }
    }
}