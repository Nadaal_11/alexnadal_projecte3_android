package com.example.projecte3mycats.ui.model.mapper


import com.example.projecte3mycats.Data.apiservice.model.CatImageDto
import com.example.projecte3mycats.Data.apiservice.model.CatsDto
import com.example.projecte3mycats.ui.model.CatsUIModel


class CatsDtoUIModelMapper {
    fun map(breeds: List<CatsDto>, images: List<CatImageDto>): List<CatsUIModel> {
        return images.mapIndexed { index, image ->
            CatsUIModel(
                breeds[index].id,
                image.imageUrl ?: "",
                breeds[index].name,
                breeds[index].temperament,
                breeds[index].countryCode,
                breeds[index].description,
                breeds[index].wikipedia_url
            )
        }
    }
}