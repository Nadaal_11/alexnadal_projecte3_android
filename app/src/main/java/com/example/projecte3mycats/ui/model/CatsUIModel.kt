package com.example.projecte3mycats.ui.model


data class CatsUIModel (
    val id: String,
    val img_url : String,
    val name: String,
    val temperament: String,
    val countryCode: String,
    val description: String,
    val wikipedia_url: String

)