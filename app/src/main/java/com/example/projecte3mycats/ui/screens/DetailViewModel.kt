package com.example.projecte3mycats.ui.screens

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.projecte3mycats.Data.apiservice.CatsApi
import com.example.projecte3mycats.ui.model.CatsUIModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailViewModel: ViewModel(){
    private val _uiState = MutableStateFlow<CatsUIModel?>(null)
    val uiState: StateFlow<CatsUIModel?> = _uiState.asStateFlow()

    fun getCatsDataFromId(catsId: String) {
        if(_uiState.value == null)
            viewModelScope.launch {
                val catsDto = CatsApi.retrofitService.getBreed(catsId)
                val image = CatsApi.retrofitService.getCatImageRx(catsId)
                _uiState.value = CatsUIModel(
                    id = catsDto.id,
                    img_url= image.first().imageUrl,
                    name = catsDto.name,
                    temperament = catsDto.temperament,
                    countryCode = catsDto.countryCode,
                    description = catsDto.description,
                    wikipedia_url = catsDto.wikipedia_url
                )
            }
    }
}


